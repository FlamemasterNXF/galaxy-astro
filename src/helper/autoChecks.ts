

export const FORBIDDEN_LINKS = [
	"https://galaxy.click",
	"https://youtube.com",
	"https://reddit.com",
	"https://incrementaldb.com",
	"https://kongregate.com",
	"https://roblox.com",

	// idk how bad it is to let the server check itself so this should be here as safe guards
	"https://localhost",
	"https://127.0.0.1",
	"https://::1",
]

export const SEEK_HELP_LINKS = [
	"https://github.com",
]

export async function checkGameLink(gameUrl: string): Promise<EmbedabilityResult> {
	if (gameUrl.startsWith("http://"))
		gameUrl = "https://" + gameUrl.substring(7);
	else if (!gameUrl.startsWith("https://"))
		return { success: false, message: "Games uploaded must use https." };

	if (gameUrl.includes("https://orteil.dashnet.org/igm"))
		return { success: false, message: "IGM games are not allowed on Galaxy due to them containing advertisements." };
	if (
		gameUrl.includes("scratch.mit.edu") ||
		gameUrl.includes("turbowarp") ||
		gameUrl.includes("penguinmod")
	)
		return { success: false, message: "Scratch submissions are temporarily not allowed due to several factors. Please check back later!" }
	if (FORBIDDEN_LINKS.some(link => gameUrl.startsWith(link)))
		return { success: false, message: "That link comes from a blocklisted site. If you think this was done in error, please contact site administrators." };
	if (SEEK_HELP_LINKS.some(link => gameUrl.includes(link)))
		return { success: false, message: "You need to provide a link to an embeddable game, not a page containing the game. If you'd like help with this, please contact site administrators." };

	const checkRes = await checkForEmbedability(gameUrl);
	if (checkRes.success) {
		gameUrl = checkRes.finalUrl;
	} else {
		return { success: false, message: "Embedability checks failed: " + checkRes.message }
	}

	return { success: true, finalUrl: gameUrl };
}



export type EmbedabilityResult = {
	success: boolean;
	finalUrl?: string;
	message?: string;
};

export async function checkForEmbedability(gameUrl: string): Promise<EmbedabilityResult> {
	try {
		let res = await fetch(gameUrl, { method: "HEAD" });
		if (res.status >= 400 && res.status <= 499) {
			return {
				success: false,
				message: "The game link you provided responded with a " + res.status + " error. Did you spell the link correctly?"	
			}
		}
		else if (res.status >= 500 && res.status <= 599) {
			return {
				success: false,
				message: "The game link you provided responded with a " + res.status + " error. This might fix itself if you try again, but it doesn't hurt to double check that your game is running as expected."	
			}
		}
		else if (res.status != 200) {
			return {
				success: false,
				message: "The game link you provided responded with a " + res.status + " status code (expected 200)."	
			}
		}
	
		let frameOptions = (res.headers.get("X-Frame-Options") ?? "").toLowerCase();
		let secPolicy = (res.headers.get("Content-Security-Policy") ?? "").toLowerCase();
		if (
			frameOptions.includes("deny") ||
			frameOptions.includes("sameorigin") ||
			// security policies that exist, don't explicitly specify a frame-ancestors property, allow all frame ancestors by default.
			(secPolicy &&
				secPolicy.includes("frame-ancestors") &&
				!/(?:^|;) *frame-ancestors (?:[^;]* )?(?:https:\/\/)?galaxy\.click(?:$|\/? |;)/.test(secPolicy))
		) {
			return {
				success: false,
				message: "Your game seems to disallow galaxy from embedding it due to an underlying web policy clause. If you're hosting your game on a game-provider website, make sure you copied the dedicated embed link provided by the website instead of the link in your browser address bar.",
			};
		}
	
		return {
			success: true,
			finalUrl: res.url	
		}
	} catch (e) {
		console.log(e);
		return {
			success: false,
			message: "Could not fetch the game page. Did you spell the link correctly?"	
		}
	}
}