import type { IForumCategory, IUser } from "~/types";
import { CategoryModerator } from "~/models/categorymod";
import { ForumCategory } from "~/models/forumcategory";
import { ForumPost } from "~/models/forumpost";
import { ForumThread } from "~/models/forumthread";
import { Game } from "~/models/game";
import { hasStaffPerm } from "./adminHelper";
import { keyv } from "~/models/keyv";

async function forumModeratorLevelCore(
	user: number,
	category: IForumCategory
): Promise<number> {
	const node = await CategoryModerator.findOne({
		category: category.id,
		user,
	});
	if (node !== null) return node.perms;
	if (category.parent !== false) {
		const parent = await ForumCategory.findOne({ id: category.parent });
		return forumModeratorLevelCore(user, parent);
	}
	return 0;
}

export async function forumModeratorLevel(
	user: IUser | null | undefined,
	category: IForumCategory
): Promise<number> {
	if (user === null || user === undefined) return 0;
	// Site staff are all-powerful on forums
	if (hasStaffPerm(user.modLevel, "mod")) return 4;
	return await forumModeratorLevelCore(user.id, category);
}

// Acquire list of moderators by recursive searching
export async function getCategoryModerators(category: IForumCategory) {
	if (category.parent === false)
		return await CategoryModerator.find({ category: category.id }, null, {
			lean: true,
		});

	const [mods, parent] = await Promise.all([
		CategoryModerator.find({ category: category.id }, null, { lean: true }),
		ForumCategory.findOne({ id: category.parent }),
	]);
	return [...mods, ...(await getCategoryModerators(parent))];
}

export async function createLinkedThread(id: number) {
	const game = await Game.findOne({ id });

	const timestamp = new Date();
	const [threadId, postId, categoryId] = await Promise.all([
		keyv.id("lastThreadId"),
		keyv.id("lastPostId"),
		keyv.get("autoGameCategory", 1),
	]);

	const thread = new ForumThread({
		author: -1,
		category: categoryId,
		id: threadId,
		when: timestamp,
		lastPost: timestamp,
		preview: `Auto-generated thread for ${game.name}`,
		name: game.name,
		postCount: 1,
		linkedGame: id,
	});
	await thread.save();

	const post = new ForumPost({
		author: -1,
		body: `This thread was automatically created to serve as a place to discuss ${game.name} on the forums. Feel free to ask questions, share your progress, or anything else related to the game!

If you are the creator of this game and would like to upgrade this thread to a category, check your game's edit page.`,
		id: postId,
		when: timestamp,
		thread: threadId,
	});

	await Promise.all([
		post.save(),
		ForumCategory.updateOne(
			{ id: categoryId },
			{
				$set: { lastPost: timestamp, lastThread: timestamp },
				$inc: { postCount: 1, threadCount: 1 },
			}
		),
	]);
}
