After finding a vulnerability, the user model was refactored to fully decouple authentication information like email and password from public information like username and playtime.

This code removes authentication-related properties from the `User` model, and adds them to a new `UserAuth` model.

`use galaxy`

`db.users.dropIndex("email_1")`

```
db.users.find({ password: { $exists: 1 } }).forEach(function (doc) {
	db.userauths.insertOne({
		id: doc.id,
		email: doc.email,
		emailVerified: doc.emailVerified,
		password: doc.password,
		passwordReset: doc.passwordReset,
		passwordResetExpiry: doc.passwordResetExpiry,
		sessionState: doc.sessionState,
	})
	db.users.updateOne({ id: doc.id }, { $unset: {
		email: 1,
		emailVerified: 1,
		password: 1,
		passwordReset: 1,
		passwordResetExpiry: 1,
		sessionState: 1
	} });
});
```