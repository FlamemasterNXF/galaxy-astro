import { connect, disconnect } from "mongoose";
import { months } from "~/helper/coppa";
import { sendMail } from "~/helper/mail";
import sendWebhook from "~/helper/sendWebhook";

await connect(process.env.DB_URL);

import { User } from "~/models/user";
import { UserAuth } from "~/models/userauth";
import { UserParent } from "~/models/userparent";

// Delete minor accounts that haven't finished authentication flow
const minorsToNuke = await UserAuth.countDocuments({
	minorNukeAccount: { $lt: Date.now() },
});

// This is an edge case to prevent the catastrophic failure scenario of somehow nuking all user accounts.
// No way this many minors would be ripe for nuking in under 30 minutes. If so, the limit can be adjusted.
if (minorsToNuke < 100) {
	await UserAuth.deleteMany({ minorNukeAccount: { $lt: Date.now() } });
} else {
	sendWebhook(process.env.STAFF_REPORT_WEBHOOK, {
		content: "Over 100 minors were set to be nuked. Please investigate",
	});
}

// Send reminder emails to parents
const parentsToEmail = await UserParent.find({
	reminderTimestamp: { $lt: Date.now() },
	reminderSent: false,
});

await Promise.all(
	parentsToEmail.map(async parent => {
		const user = await User.findOne({ id: parent.user });
		const userAuth = await UserAuth.findOne({ id: parent.user });
		await sendMail(
			parent.parentEmail,
			"Reminder of account approval on galaxy.click",
			{
				plaintext: `Hello,

This email is a reminder that 24 hours ago you approved your child's request to create an account on galaxy.click.

Their account information is as follows:
username -- ${user.name}
email address -- ${userAuth.email}
birth month -- ${months[userAuth.birthMonth]} ${userAuth.birthYear}
If you performed this action, no further action is necessary.

If you would ever like to revoke your permission by **permanently deleting** your child's account, click the link below.

https://galaxy.click/coppa?code=${parent.parentCode}&action=1

For further inquiries, please contact support@galaxy.click.`,
				html: `<p>Hello,</p>
<p>This email is a reminder that 24 hours ago you approved your child's request to create an account on galaxy.click.</p>
<p>Their account information is as follows:<br />
username -- ${user.name}<br />
email address -- ${userAuth.email}<br />
birth month -- ${months[userAuth.birthMonth]} ${userAuth.birthYear}</p>
<p>If you performed this action, no further action is necessary.</p>
<p>If you would ever like to revoke your permission by <b>permanently deleting</b> your child's account, click <a href="https://galaxy.click/coppa?code=${parent.parentCode}&action=1">this link</a>.</p>
<p>For further inquiries, please contact support@galaxy.click.</p>
`,
			}
		);
		parent.reminderSent = true;
		await parent.save();
	})
);

await disconnect();

console.log("coppa-housekeeping successful.");
console.log("Current time: " + new Date());
