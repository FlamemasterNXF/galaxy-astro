import { Schema, model } from "mongoose";
import type { IUser } from "~/types";

const UserSchema = new Schema<IUser>(
	{
		name: {
			type: String,
			required: true,
			minLength: 3,
			maxLength: 28,
			// Commented out because all hardnuked accounts have the same name
			// (that being [removed])
			// unique: true,
		},
		id: Number,
		pfpTimestamp: Number,

		bio: {
			type: String,
			maxLength: 1024,
			default: "",
		},

		flairs: {
			type: [String],
			default: ["none"],
		},
		equippedFlair: {
			type: String,
			default: "none",
		},

		playMinutes: {
			type: Number,
			default: 0,
		},
		lastHeartbeat: {
			type: Number,
			default: 0,
		},

		// Administrator stuff :)
		// MOD LEVELS ARE NOT LINEAR!
		// 0: User
		// 1: Mod
		// 2: Admin
		// 3: Owner
		// 4. Game verifier
		modLevel: {
			type: Number,
			default: 0,
		},
		banned: {
			type: Schema.Types.Mixed,
			default: false,
		},
		muted: {
			type: Boolean,
			default: false,
		},
		blockList: {
			type: [Number],
			default: [],
		},
	},
	{
		toJSON: {
			transform(doc, ret, _options) {
				delete ret.blockList;
				delete ret.muted;
				delete ret.banned;
				delete ret._id;
				delete ret.__v;
				return ret;
			},
		},
		collation: {
			locale: "en",
			strength: 2,
		},
	}
);

export const User = model<IUser>("User", UserSchema);
