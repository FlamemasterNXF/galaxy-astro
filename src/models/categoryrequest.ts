import { Schema, model } from "mongoose";
import type { ICategoryRequest } from "~/types";

const CategoryRequestSchema = new Schema<ICategoryRequest>(
	{
		name: String,
		slug: String,
		description: String,
		reason: String,
		requestAuthor: Number,
		resolved: {
			type: Boolean,
			default: false,
		},
	},
	{
		timestamps: true,
	}
);

export const CategoryRequest = model<ICategoryRequest>(
	"CategoryRequests",
	CategoryRequestSchema
);
