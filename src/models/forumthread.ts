import { type PaginateModel, Schema, model } from "mongoose";
import type { IForumThread } from "~/types";
import paginate from "mongoose-paginate-v2";

const ForumThreadSchema = new Schema<IForumThread>({
	id: Number,
	name: String,
	preview: String,
	author: Number,
	when: {
		type: Date,
		default: () => new Date(),
	},
	postCount: {
		type: Number,
		default: 0,
	},
	lastPost: {
		type: Date,
		default: new Date(0),
	},
	category: Number,
	pinned: {
		type: Boolean,
		default: false,
	},
	locked: {
		type: Boolean,
		default: false,
	},
	deleted: {
		type: Boolean,
		default: false,
	},
	linkedGame: {
		type: Schema.Types.Mixed,
		default: false,
	},
	solution: {
		type: Schema.Types.Mixed,
		default: false,
	},
});

ForumThreadSchema.plugin(paginate);

export const ForumThread = model<IForumThread, PaginateModel<IForumThread>>(
	"ForumThreads",
	ForumThreadSchema
);
