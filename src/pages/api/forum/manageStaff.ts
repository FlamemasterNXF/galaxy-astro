import { rdata, rmsg } from "~/helper/res";
import { CategoryModerator } from "~/models/categorymod";
import { ForumCategory } from "~/models/forumcategory";
import { forumModeratorLevel } from "~/helper/forum";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	user: z.number().int(),
	newLevel: z.number().int().max(3).min(0),
	category: z.number().int(),
});

export const POST = route(
	{ auth: true, notMuted: true, schema },
	async ({ locals }, req) => {
		const { user } = locals.auth;
		const { user: promotingUserId, newLevel, category } = req;

		const catDoc = await ForumCategory.findOne({ id: category });
		if (!catDoc) return rmsg("Category not found", 400);

		const modLevel = await forumModeratorLevel(user, catDoc);
		if (modLevel <= 1) return rmsg("NO", 400);
		if (newLevel >= modLevel)
			return rmsg("You lack permissions to promote to that level", 400);

		const currentMod = await CategoryModerator.findOne({
			category,
			user: promotingUserId,
		});
		if ((currentMod?.perms ?? 0) >= modLevel)
			return rmsg("That user is too powerful for you to modify", 400);

		// Delete doc because they drop down to 0 perms (equivalent to no doc)
		if (newLevel === 0) {
			await CategoryModerator.deleteOne({
				user: promotingUserId,
				category,
			});
		} else {
			// Perms are non-zero!
			// This one query updates the document OR creates it if it doesn't exist.
			// (the upsert: true makes it that way)
			await CategoryModerator.updateOne(
				{ category, user: promotingUserId },
				{
					$set: {
						perms: newLevel,
					},
				},
				{ upsert: true }
			);
		}

		return rdata({ message: "ok" });
	}
);
