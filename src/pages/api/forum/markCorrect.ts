import { ForumCategory } from "~/models/forumcategory";
import { ForumPost } from "~/models/forumpost";
import { ForumThread } from "~/models/forumthread";
import { forumModeratorLevel } from "~/helper/forum";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.number().int();

export const POST = route(
	{ auth: true, notMuted: true, schema },
	async ({ locals }, postId) => {
		const { user } = locals.auth;

		const post = await ForumPost.findOne({
			id: postId,
			deleted: false,
		});
		if (!post) return rmsg("Thread not found", 400);

		const thread = await ForumThread.findOne({ id: post.thread });
		const category = await ForumCategory.findOne({ id: thread.category });
		const modLevel = await forumModeratorLevel(user, category);

		if (modLevel === 0 && thread.author !== user.id) {
			return rmsg(
				"You do not have permissions to mark the correct post in this thread",
				400
			);
		}

		thread.solution = postId;
		await thread.save();

		return rmsg("ok");
	}
);
