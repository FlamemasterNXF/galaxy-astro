// I don't think this endpoint is needed for the time being, but I'll leave it in the codebase.
// TODO: Does this need to be deleted?

import { GameRequest } from "~/models/gamerequest";
import { User } from "~/models/user";
import { adminRoute } from "~/helper/route";
import { rmsg } from "~/helper/res";
import { z } from "zod";

const schema = z.object({
	oid: z.string(),
	flair: z.boolean(),
});

export const POST = adminRoute({ schema }, async (_request, { oid, flair }) => {
	const request = await GameRequest.findById(oid);
	if (!request) return rmsg("Game request not found", 404);

	request.resolved = true;
	request.successful = flair;
	await request.save();

	if (flair === true) {
		const user = await User.findOne({ id: request.requestAuthor });
		if (!user.flairs.includes("suggester"))
			await User.updateOne(
				{ id: request.requestAuthor },
				{ $push: { flairs: "suggester" } }
			);
	}
});
