import { User } from "~/models/user";
import { adminRoute } from "~/helper/route";
import { rmsg } from "~/helper/res";
import sharp from "sharp";
import { updatePfps } from "~/helper/img";
import { z } from "zod";

const schema = z.object({
	user: z.number().int(),
});

export const POST = adminRoute({ schema }, async (_request, { user }) => {
	const u = await User.findOne({ id: user });
	if (!u) return rmsg("User not found", 404);

	const original = u.pfpTimestamp;
	u.pfpTimestamp = Date.now();
	await u.save();

	await updatePfps(
		sharp("public/unknown-user.webp"),
		user,
		u.pfpTimestamp,
		original
	);
});
