import { Game } from "~/models/game";
import { UnverifiedGameData } from "~/models/unverifiedgamedata";
import { adminDataRoute } from "~/helper/route";

export const GET = adminDataRoute(
	{
		reqLevel: "verifier",
	},
	async () => {
		const unverified = await Game.find({ verified: { $ne: true } });

		const rawUnverifiedData = await UnverifiedGameData.find({
			id: { $in: unverified.map(game => game.id) },
		});

		const unverifiedData = {};
		rawUnverifiedData.forEach(data => {
			unverifiedData[data.id] = data;
		});

		return { games: unverified, data: unverifiedData };
	}
);
