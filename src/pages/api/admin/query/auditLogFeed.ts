import { StaffActivity } from "~/models/staffactivity";
import { adminDataRoute } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	page: z.number().int().min(1).default(1),
	user: z.number().int().optional(),
});

export const POST = adminDataRoute(
	{
		reqLevel: "any",
		schema,
	},
	async (_request, data) => {
		const query = data.user !== undefined ? { staff: data.user } : {};
		const pageCount = Math.ceil(
			(await StaffActivity.countDocuments(query)) / 100
		);
		const result = (
			await StaffActivity.find(query)
				.sort("-createdAt")
				.limit(100)
				.skip(100 * (data.page - 1))
		).map(x => x.toObject());

		return { result, pageCount };
	}
);
