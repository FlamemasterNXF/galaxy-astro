import { Game } from "~/models/game";
import { UnverifiedGameData } from "~/models/unverifiedgamedata";
import { adminRoute } from "~/helper/route";
import { logAction } from "~/helper/adminHelper";
import { z } from "zod";

const schema = z.object({
	id: z.number().int(),
	up: z.boolean(),
});

export const POST = adminRoute(
	{ reqLevel: "verifier", schema },
	async (_request, { id, up }, mod) => {
		const data = await UnverifiedGameData.findOne({ id });
		if (!data) return;

		const wasUp = data.upvotes.includes(mod.id);
		const wasDown = data.downvotes.includes(mod.id);

		data.upvotes = data.upvotes.filter(x => x !== mod.id);
		data.downvotes = data.downvotes.filter(x => x !== mod.id);

		if (up === true && !wasUp) {
			data.upvotes.push(mod.id);
		} else if (up === false && !wasDown) {
			data.downvotes.push(mod.id);
		}

		await data.save();

		const game = await Game.findOne({ id });
		const voteState = data.upvotes.includes(mod.id)
			? "up"
			: data.downvotes.includes(mod.id)
				? "down"
				: "none";
		await logAction(
			mod.id,
			"game",
			`set vote on ${game.name} (${game.id}) to ${voteState}`,
			`leaving game with ${data.upvotes.length} upvotes, and ${data.downvotes.length} downvotes`,
			{ game, data }
		);
	}
);
