import { Comment } from "~/models/comment";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	id: z.number().int(),
	vote: z.literal(1).or(z.literal(-1)),
});

export const POST = route(
	{
		auth: true,
		notMuted: true,
		notUnderage: true,
		schema,
	},
	async ({ locals }, { id: commentId, vote }) => {
		const { user } = locals.auth;

		const comment = await Comment.findOne({
			id: commentId,
			deleted: false,
		});
		if (!comment)
			return rmsg("Attempted to vote on an unknown comment", 404);

		const voter = user.id;
		let type: "up" | "down";
		let other: "up" | "down";

		switch (vote) {
			case 1:
				type = "up";
				other = "down";
				break;
			case -1:
				type = "down";
				other = "up";
				break;
		}

		if (comment[type].includes(voter)) {
			// Retract a vote
			comment[type] = comment[type].filter(id => id !== voter);
			comment[`${type}Count`]--;
		} else if (comment[other].includes(voter)) {
			// Swap the vote
			comment[other] = comment[other].filter(id => id !== voter);
			comment[`${other}Count`]--;

			comment[`${type}Count`]++;
			comment[type].push(voter);
		} else {
			// First time voting on comment
			comment[`${type}Count`]++;
			comment[type].push(voter);
		}

		comment.score = comment.upCount - comment.downCount;
		await comment.save();

		return rmsg("success");
	}
);
