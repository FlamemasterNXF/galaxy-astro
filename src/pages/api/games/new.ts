import { Game, gameSchema } from "~/models/game";
import { byLockdown, route } from "~/helper/route";
import { rdata, rmsg } from "~/helper/res";
import { UnverifiedGameData } from "~/models/unverifiedgamedata";
import { checkGameLink } from "~/helper/autoChecks";
import { env } from "~/helper/env";
import sendWebhook from "~/helper/sendWebhook";
import sharp from "sharp";
import { updateThumbs } from "~/helper/img";

export const POST = route(
	{
		disabled: byLockdown(
			"gamesLocked",
			"The creation of new games is disabled at this moment."
		),
		auth: true,
		notMuted: true,
		notUnderage: true,
		schema: gameSchema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const check = await checkGameLink(req.link);
		if (check.success) {
			req.link = check.finalUrl;
		} else {
			return rmsg(check.message, 400);
		}

		// Find an existing game
		let game = await Game.findOne({ name: req.name });
		if (game !== null)
			return rmsg("A game already exists with that name.", 400);

		game = await Game.findOne({ link: req.link });
		if (game !== null) rmsg("A game already exists with that link.", 400);

		// const lastGameId = await keyv.id("lastGameId");

		game = new Game({
			name: req.name,
			description: req.description ?? "No description provided.",
			link: req.link,
			// A large, negative random number is assigned to unverified games so
			// they don't take up regular IDs and leave gaps when rejected.
			id: Math.floor(Math.random() * Number.MIN_SAFE_INTEGER - 1),
			author: user.id,

			unlisted: req.private || req.unlisted,
			// Dirty AF boolean conversion
			private: !!req.private,
		});
		game.thumbTimestamp = Date.now();
		await game.save();

		// Yes, I know I'm not awaiting this. So what?
		updateThumbs(
			sharp("public/unknown-game.webp"),
			game.id,
			game.thumbTimestamp,
			0
		);

		if (env.STAFF_GAME_WEBHOOK !== undefined)
			await sendWebhook(env.STAFF_GAME_WEBHOOK, {
				content: `New game ${game.name} needs verification:\n<https://galaxy.click/play/${game.id}>\n<https://galaxy.click/admin>`,
			});

		const unverifiedData = new UnverifiedGameData({
			id: game.id,
			upvotes: [],
			downvotes: [],
			chat: [],
			read: [],
		});
		await unverifiedData.save();

		return rdata({ message: "Thanks for submitting!", id: game.id }, 200);
	}
);
