import { rfu, rmsg } from "~/helper/res";
import { ChatMessage } from "~/models/chatmessage";
import { Comment } from "~/models/comment";
import { ForumPost } from "~/models/forumpost";
import { ForumThread } from "~/models/forumthread";
import { Game } from "~/models/game";
import { Message } from "~/models/message";
import { Report } from "~/models/report";
import { User } from "~/models/user";
import { env } from "~/helper/env";
import { route } from "~/helper/route";
import sendWebhook from "~/helper/sendWebhook";
import { z } from "zod";

const schema = z.object({
	part: z.string(),
	reason: z.string(),
	data: z.any(),
});

export const POST = route(
	{
		auth: true,
		notMuted: true,
		notUnderage: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		let data = {};
		switch (req.data?.type) {
			case "comment": {
				const comment = await Comment.findOne({
					id: parseInt(req.data.id),
					deleted: false,
				});
				if (!comment) return rmsg("Invalid comment", 400);

				data = comment;
				break;
			}
			case "game": {
				const game = await Game.findOne({ id: parseInt(req.data.id) });
				if (!game) return rmsg("Invalid game", 400);

				data = game;
				break;
			}
			case "user": {
				const user = await User.findOne({ id: parseInt(req.data.id) });
				if (!user) return rmsg("Invalid user", 400);
				// system user
				if (user.id === -1)
					return rfu("Cannot report system user.", 403);

				data = user;
				break;
			}
			case "chat": {
				const msg = await ChatMessage.findOne({
					_id: req.data.mid,
					deleted: false,
				});
				if (!msg) return rmsg("Invalid message", 400);

				let msgBeforeContext = await ChatMessage.find({
					createdAt: { $lt: msg.createdAt },
					channel: msg.channel,
				})
					.sort({
						createdAt: -1,
					})
					.limit(20);
				msgBeforeContext = msgBeforeContext.reverse();

				const msgAfterContext = await ChatMessage.find({
					createdAt: { $gt: msg.createdAt },
					channel: msg.channel,
				})
					.sort({
						createdAt: 1,
					})
					.limit(20);

				data = {
					before: msgBeforeContext,
					after: msgAfterContext,
					msg,
				};
				break;
			}
			case "message": {
				const msg = await Message.findOne({
					_id: req.data._id,
					to: user.id,
					deleted: false,
				});
				if (!msg) return rmsg("Invalid message", 400);

				data = msg;
				break;
			}
			case "thread": {
				const thread = await ForumThread.findOne({
					id: req.data.id,
					deleted: false,
				});
				if (!thread) return rmsg("Invalid thread", 400);
				data = thread;
				break;
			}
			case "post": {
				const post = await ForumPost.findOne({
					id: req.data.id,
					deleted: false,
				});
				if (!post) return rmsg("Invalid post", 400);
				data = post;
				break;
			}
			default:
				return rmsg("Invalid report type", 400);
		}

		const report = new Report({
			author: user.id,
			part: req.part,
			reason: req.reason,
			type: req.data.type,
			data,
		});

		await report.save();

		if (env.STAFF_REPORT_WEBHOOK !== undefined)
			await sendWebhook(env.STAFF_REPORT_WEBHOOK, {
				content: `new report | <https://galaxy.click/admin/>\n${report.part} | ${report.reason}\n`,
			});

		return rmsg("Thanks for helping to keep Galaxy safe!");
	}
);
