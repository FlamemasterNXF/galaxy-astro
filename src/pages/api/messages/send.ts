import { byLockdown, route } from "~/helper/route";
import { rfu, rmsg } from "~/helper/res";
import { Message } from "~/models/message";
import { User } from "~/models/user";
import { UserAuth } from "~/models/userauth";
import { userUnder13 } from "~/helper/coppa";
import { z } from "zod";

const schema = z.object({
	content: z.string().max(16_384),
	title: z.string().max(128),
	to: z.number().int(),
});

export const POST = route(
	{
		disabled: byLockdown(
			"messagesLocked",
			"Sending messages is disabled at this moment."
		),
		auth: true,
		notMuted: true,
		notUnderage: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const to = await User.findOne({ id: req.to });
		if (!to) return rmsg("Recipient doesn't exist", 400);
		if (to.banned !== false) return rmsg("Recipient is banned", 400);
		if (to.id === -1) return rfu("You can't do that...", 403);

		// Silently fail when attempting to send a message to an underage user
		const toAuth = await UserAuth.findOne({ id: to.id });
		if (userUnder13(toAuth)) return rmsg("success");

		const msg = new Message({
			from: user.id,
			to: to.id,
			title: req.title,
			content: req.content,
		});
		await msg.save();

		return rmsg("success");
	}
);
