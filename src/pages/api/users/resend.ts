import { UserAuth } from "~/models/userauth";
import { randomString } from "~/helper/random";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { sendVerifyMail } from "~/helper/mail";

export const POST = route({}, async ({ locals }) => {
	const { needsVerification, user } = locals.auth;
	if (!needsVerification) return rmsg("You're not allowed to do that", 400);

	const foundUser = await UserAuth.findOne({ id: user.id });
	foundUser.emailVerified = randomString(32);
	await foundUser.save();

	await sendVerifyMail(foundUser.email, foundUser.emailVerified);

	return rmsg("Resent!", 200);
});
