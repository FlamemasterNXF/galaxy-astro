import { AF2024Participant } from "~/models/af2024participant";
import { ChatMessage } from "~/models/chatmessage";
import { Comment } from "~/models/comment";
import { Favorite } from "~/models/favorite";
import { ForumPost } from "~/models/forumpost";
import { ForumThread } from "~/models/forumthread";
import { Game } from "~/models/game";
import { GameSave } from "~/models/gamesave";
import { PlaySession } from "~/models/playsession";
import { Playtime } from "~/models/playtime";
import { Rating } from "~/models/rating";
import { User } from "~/models/user";
import { UserAuth } from "~/models/userauth";
import { UserParent } from "~/models/userparent";
import bcrypt from "bcryptjs";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	password: z
		.string({
			invalid_type_error: "Password not provided",
		})
		.min(3, "Password is too short (min: 3)")
		.max(128, "Password is too long (max: 128)"),
});

export const POST = route(
	{
		auth: true,
		schema,
	},
	async ({ locals, cookies }, req) => {
		const { user } = locals.auth;

		if (user.muted !== false || user.banned !== false)
			return rmsg(
				"Because you are muted or banned, you can't automatically delete your account.",
				400
			);

		const publishedGameCount = await Game.countDocuments({
			author: user.id,
		});
		if (publishedGameCount > 0)
			return rmsg(
				"You can not delete your account while you have published games.",
				400
			);

		const userDoc = await UserAuth.findOne({ id: user.id });

		const isCorrect = await bcrypt.compare(req.password, userDoc.password);
		if (!isCorrect) return rmsg("Password is not correct", 400);

		// TO DELETE:
		// - GameSave
		// - ChatMessage
		// - Comments
		// - ForumThread
		// - ForumPost
		// - Rating
		//   - Recompute isn't needed, a new rating being given will do that
		// - Favorite
		//   - Recompute game faves
		// - User
		// - UserAuth
		// - AF2024 stuff
		await GameSave.deleteMany({ user: user.id });
		await ChatMessage.deleteMany({ user: user.id });
		await Comment.deleteMany({ author: user.id });
		await ForumThread.deleteMany({ author: user.id });
		await ForumPost.deleteMany({ author: user.id });
		await Rating.deleteMany({ author: user.id });

		const faves = await Favorite.find({ user: user.id });
		const favesIds = faves.map(fave => fave.game);
		await Game.updateMany(
			{ id: { $in: favesIds } },
			{ $inc: { favorites: -1 } }
		);
		await Favorite.deleteMany({ user: user.id });
		await AF2024Participant.deleteOne({ user: user.id });

		// o7
		await User.deleteOne({ id: user.id });
		await UserAuth.deleteOne({ id: user.id });
		await UserParent.deleteOne({ user: user.id });

		// TO ANONYMIZE (by setting to random, large negative number):
		// - Playtime
		// - PlaySession
		const ghostID =
			Math.floor(
				Math.random() * (Number.MAX_SAFE_INTEGER - 1000000) * -1
			) - 1000000;
		await Playtime.updateMany({ user: user.id }, { user: ghostID });
		await PlaySession.updateMany({ user: user.id }, { user: ghostID });

		cookies.delete("token", { path: "/" });

		return new Response(
			JSON.stringify({
				message: "Woah...",
			}),
			{
				status: 200,
			}
		);
	}
);
