import { UserAuth, fromEmail } from "~/models/userauth";
import { rdata, rmsg } from "~/helper/res";
import { User } from "~/models/user";
import { randomString } from "~/helper/random";
import { route } from "~/helper/route";
import { sendMail } from "~/helper/mail";
import { z } from "zod";

const schema = z.object({
	email: z
		.string()
		.email("You need a valid email to reset the password of an account")
		.or(z.literal("(already logged in)")),
});

export const POST = route(
	{
		schema,
	},
	async ({ locals: { auth } }, req) => {
		const userAuth = await (auth.loggedIn
			? UserAuth.findOne({ id: auth.user.id })
			: fromEmail(req.email));
		if (!userAuth) return rmsg("User not found", 400);
		const user = await User.findOne({ id: userAuth.id });

		userAuth.passwordReset = randomString(32);
		userAuth.passwordResetExpiry = Date.now() + 15 * 1000 * 1000;

		await userAuth.save();

		// /reset?key=user.passwordReset

		await sendMail(userAuth.email, "galaxy.click password reset", {
			plaintext: `
-=<[ galaxy password reset ]>=-

A password reset request was started on your account <b>@${user.name}</b> on <b>galaxy.click</b>

The link to reset your password is: https://galaxy.click/reset?key=${userAuth.passwordReset}

This link expires 15 minutes after this email is sent.

If you did not request this action, you can safely ignore this email.
			`.trim(),
			html: `
				<h1>galaxy password reset</h1>
				<p>
					A password reset request was started on your account <b>@${user.name}</b> on <b>galaxy.click</b>
				</p>
				<p>
					The link to reset your password is: 
					<a href="https://galaxy.click/reset?key=${userAuth.passwordReset}">https://galaxy.click/reset?key=${userAuth.passwordReset}</a>
				</p>
				<p>
					This link expires 15 minutes after this email is sent.
				</p>
				<p>
					If you did not request this action, you can safely ignore this email.
				</p>
			`.trim(),
		});

		return rdata({ message: "Email sent!", done: true });
	}
);
