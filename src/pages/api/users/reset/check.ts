import { rdata, rmsg } from "~/helper/res";
import { User } from "~/models/user";
import { UserAuth } from "~/models/userauth";
import { route } from "~/helper/route";

export const GET = route({}, async ({ url }) => {
	const token = url.searchParams.get("token");
	if (token == null) return rmsg("no");

	const userAuth = await UserAuth.findOne({
		passwordReset: token,
		passwordResetExpiry: { $gt: Date.now() },
	});
	if (userAuth === null) return rmsg("no");

	const user = await User.findOne({ id: userAuth.id });

	return rdata({ message: "yes", id: user.id, name: user.name });
});
