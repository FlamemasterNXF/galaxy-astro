import { UserAuth, generateTokenHeaders } from "~/models/userauth";
import bcrypt from "bcryptjs";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	token: z.string({
		invalid_type_error: "Invalid token",
	}),
	password: z
		.string({
			invalid_type_error: "Invalid password",
		})
		.min(10, "New password is too short (min: 10)")
		.max(128, "New password is too long (max: 128)"),
});

export const POST = route(
	{
		schema,
	},
	async (_context, req) => {
		const userAuth = await UserAuth.findOne({
			passwordReset: req.token,
			passwordResetExpiry: { $gt: Date.now() },
		});
		if (!userAuth) return rmsg("User not found", 400);

		userAuth.passwordReset = false;
		userAuth.password = await bcrypt.hash(req.password, 12);
		userAuth.sessionState++;

		await userAuth.save();

		return new Response(
			JSON.stringify({
				message: "Password updated!",
				done: true,
			}),
			{
				status: 200,
				headers: generateTokenHeaders(userAuth),
			}
		);
	}
);
