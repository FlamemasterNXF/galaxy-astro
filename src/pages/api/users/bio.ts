import { User } from "~/models/user";
import { env } from "~/helper/env";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import sendWebhook from "~/helper/sendWebhook";
import { z } from "zod";

const schema = z.object({
	bio: z
		.string({
			invalid_type_error: "Invalid bio.",
			required_error: "No bio provided.",
		})
		.max(1024, "Bios are limited to 1,024 characters."),
});

const BANNED_EARLY_TERMS = ["http", "www", ".com", "url", "href"];

export const POST = route(
	{
		auth: true,
		notMuted: true,
		notUnderage: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		if (
			user.playMinutes < 10 &&
			BANNED_EARLY_TERMS.some(term => req.bio.includes(term))
		) {
			sendWebhook(env.STAFF_REPORT_WEBHOOK, {
				content: `New account w/ ID \`${user.id}\` tried setting their bio to this, which was blocked. Worth investigating?
${req.bio}`,
			});
			return rmsg(
				"To prevent spam, brand new accounts can't have certain things in their bio. Why don't you try playing some games first?",
				400
			);
		}

		const dbU = await User.findOne({ id: user.id });
		dbU.bio = req.bio;
		await dbU.save();

		return rmsg("Bio updated!");
	}
);
