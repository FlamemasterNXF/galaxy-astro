// Temporary experimentation file.
// If this exists a month after 2024/09/08, it's probably a mistake.
// Feel free to open an MR to remove it!
// See also: UserManager.svelte

import { env } from "~/helper/env";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import sendWebhook from "~/helper/sendWebhook";
import { writeFile } from "fs/promises";
import { z } from "zod";

const schema = z.object({
	bio: z.string().max(1024),
	apples: z.number(),
});

export const POST = route(
	{
		auth: true,
		notMuted: true,
		schema,
	},
	async ({ locals, request, clientAddress }, req) => {
		const { user } = locals.auth;
		const stamp = Date.now();

		const fancyRequest = {};
		for (const key in request) {
			if (key === "headers")
				fancyRequest.headers = Object.fromEntries(
					request.headers.entries()
				);
			else fancyRequest[key] = request[key];
		}

		await writeFile(
			`${user.name}-${stamp}.txt`,
			JSON.stringify({
				...req,
				clientAddress,
				request: fancyRequest,
			})
		);

		sendWebhook(env.STAFF_REPORT_WEBHOOK, {
			content: `Bio trap tripped with user \`${user.id}\` (${user.name}). See ${user.name}-${stamp}.txt`,
		});

		return rmsg("Bio updated!");
	}
);
