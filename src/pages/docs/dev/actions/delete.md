---
layout: ~/layouts/Doc.astro
title: galaxy docs - delete action
---


# `delete`

The `delete` action sends a request to Galaxy to delete the cloud save data from a certain save slot. Success of the request will then be reported through the [`deleted`](/docs/dev/responses/deleted) response.

## returning response

- [`deleted`](/docs/dev/responses/deleted)

## parameters

- **`slot`** *(number)*  
  The save slot number. Must be an integer between 0 and 10, inclusive.

## example

```js
window.top.postMessage({
	action: "delete",
	slot: 0,
}, "https://galaxy.click");
```
