---
layout: ~/layouts/Doc.astro
title: galaxy docs - flairs
---

# flairs ✨

Flairs are a simple way of showing someone's status on Galaxy. You can have one flair equipped at a time.

The current flairs (and their requirements) are:

- **`none`**  
  The default "flair", in which no flair appears.

- **`gamedev`**  
  Users that posted at least one game of theirs on Galaxy. This flair is automatically given to you after one of your games is verified by our staff.
  
- **`suggester`**  
  Users that have successfully requested a game and had the author of said game post it onto Galaxy. (Note: Game requests are currently paused at the moment.)

- **`counter`**
  Users that clicked at least once in the [2024 April Fools' Day event](https://galaxy.click/counter).

- **`countest`**
  Users that clicked at least 1,000 times in the [2024 April Fools' Day event](https://galaxy.click/counter).

- **`tester`**  
  Users that helped test Galaxy in its infancy.

- **`contributor`**  
  Users that made significant non-monetary contributions (e.g. code) to Galaxy.

- **`donator`**  
  Users that financially supported Galaxy on [Patreon](https://patreon.com/Yhvr).

- **`verifier`**
  Verifiers of Galaxy. These users assist moderators in manually reviewing games, ensuring the process always moves smoothly.

- **`mod`**  
  Moderators of Galaxy. These users help keep this website safe and clean by manually reviewing games and removing offensive content.

- **`admin`**  
  Administrators of Galaxy. These users have the same privileges as moderators, plus more.

- **`owner`**  
  The owner of Galaxy. There are only 2 users with this flair: [yhvr](/user/1), the founder of Galaxy, and [Galaxy](/user/-1) itself.
