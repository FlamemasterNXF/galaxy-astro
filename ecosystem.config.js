export default {
	apps: [
		{
			name: "express",
			script: "./express.js",
			autorestart: true,
			instances: 1,
		},
		{
			name: "ws",
			script: "./ws.js",
			autorestart: true,
			instances: 1,
		},
		{
			name: "coppa",
			script: "./coppa-housekeeping.ts",
			autorestart: false,
			instances: 1,
			cron_restart: "0,30 * * * *",
			interpreter: "bun",
			interpreter_args: "run",
		},
	],
};